
for YEAR in {1850..2100}
do
if ((${YEAR}<=2015))
  then
    echo 'true'
else
    echo 'false'
fi
done

ap.add_argument('-i', '--input', type=str,
                help="input directory, input data root path, string")

ap.add_argument('-o', '--output', type=str,
                help="output directory, needs to exist, string")

ap.add_argument('-t', '--tlh', type=bool,
                help="shall the treeline be calculated, boolean")

ap.add_argument('-s', '--ssp', type=str,
                help="ssp name, string")

ap.add_argument('-m', '--model', type=str,
                help="modelname, string")

ap.add_argument('-tt', '--time', type=str,
                help="timeframe, string")



    
INPUT='/storage/karger/chelsa_V2/scaled/' #'/storage/karger/chelsa_v2/cmip6_5km_mcb/'
OUTPUT='/storage/karger/chelsa_V2/cmip6_5km_mcb/1km/'
SSP='ssp126'
MODEL='gfdl-esm4'
TIME='2041-2070'

singularity exec -B /storage /home/karger/singularity/chelsa_V2.1.ismip.cont python ~/scripts/chelsa_cmip6_mcb/classes/get_treelim_1km_CMIP6.py --input ${INPUT} --output ${OUTPUT} --ssp ${SSP} --model ${MODEL} --time ${TIME}


for YEAR in 2013 2016
do
singularity exec -B /storage /home/karger/singularity/chelsa_V2.1.ismip.cont python ~/scripts/chelsa_cmip6_mcb/classes/get_treelim_1km.py -y ${YEAR} -i '/storage/karger/chelsa_V2/scaled/' -o '/home/karger/scratch/' -t True
done
