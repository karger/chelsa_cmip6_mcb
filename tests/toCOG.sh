
# set the fixed parameters
ISIMIP='/storage/karger/ISIMIP3b/'
OUTPUT='/storage/karger/chelsa_V2/cmip6_5km_mcb/5km/'

for YEAR in {1850..2100}
do
echo ${YEAR}

if ((${YEAR}<2016))
  then
  EXPERIMENT_ID='historical'
  for SOURCE_ID in 'ukesm1-0-ll' 'gfdl-esm4' 'ipsl-cm6a-lr' 'mpi-esm1-2-hr' 'mri-esm2-0'
  do
    FILE=/storage/karger/chelsa_V2/cmip6_5km_mcb/COG/${SOURCE_ID}_historical_gsl_${YEAR}.V.2.1.tif
    if test -f "$FILE"; then
        echo "$FILE exists."
    else
        gdal_translate -of COG /storage/karger/chelsa_V2/cmip6_5km_mcb/5km/${SOURCE_ID}_historical_gsl_${YEAR}.V.2.1.tif /storage/karger/chelsa_V2/cmip6_5km_mcb/COG/${SOURCE_ID}_historical_gsl_${YEAR}.V.2.1.tif 
    fi

    FILE=/storage/karger/chelsa_V2/cmip6_5km_mcb/COG/${SOURCE_ID}_historical_gst_${YEAR}.V.2.1.tif
    if test -f "$FILE"; then
        echo "$FILE exists."
    else
        gdal_translate -of COG /storage/karger/chelsa_V2/cmip6_5km_mcb/5km/${SOURCE_ID}_historical_gst_${YEAR}.V.2.1.tif /storage/karger/chelsa_V2/cmip6_5km_mcb/COG/${SOURCE_ID}_historical_gst_${YEAR}.V.2.1.tif 
    fi

    FILE=/storage/karger/chelsa_V2/cmip6_5km_mcb/COG/${SOURCE_ID}_historical_tmin_${YEAR}.V.2.1.tif
    if test -f "$FILE"; then
        echo "$FILE exists."
    else
        gdal_translate -of COG /storage/karger/chelsa_V2/cmip6_5km_mcb/5km/${SOURCE_ID}_historical_tmin_${YEAR}.V.2.1.tif /storage/karger/chelsa_V2/cmip6_5km_mcb/COG/${SOURCE_ID}_historical_tmin_${YEAR}.V.2.1.tif 
    fi

    FILE=/storage/karger/chelsa_V2/cmip6_5km_mcb/COG/${SOURCE_ID}_historical_tlh_${YEAR}.V.2.1.tif
    if test -f "$FILE"; then
        echo "$FILE exists."
    else
        gdal_translate -of COG /storage/karger/chelsa_V2/cmip6_5km_mcb/5km/${SOURCE_ID}_historical_tlh_${YEAR}.V.2.1.tif /storage/karger/chelsa_V2/cmip6_5km_mcb/COG/${SOURCE_ID}_historical_tlh_${YEAR}.V.2.1.tif 
    fi
  done
else
  for SOURCE_ID in 'gfdl-esm4' 'ipsl-cm6a-lr' 'mpi-esm1-2-hr' 'mri-esm2-0' 'ukesm1-0-ll'
  do
    for EXPERIMENT_ID in 'ssp370' 'ssp126'  'ssp585'
    do
        FILE=/storage/karger/chelsa_V2/cmip6_5km_mcb/COG/${SOURCE_ID}_${EXPERIMENT_ID}_gsl_${YEAR}.V.2.1.tif
        if test -f "$FILE"; then
            echo "$FILE exists."
        else
            gdal_translate -of COG /storage/karger/chelsa_V2/cmip6_5km_mcb/5km/${SOURCE_ID}_${EXPERIMENT_ID}_gsl_${YEAR}.V.2.1.tif /storage/karger/chelsa_V2/cmip6_5km_mcb/COG/${SOURCE_ID}_${EXPERIMENT_ID}_gsl_${YEAR}.V.2.1.tif 
        fi

        FILE=/storage/karger/chelsa_V2/cmip6_5km_mcb/COG/${SOURCE_ID}_${EXPERIMENT_ID}_gst_${YEAR}.V.2.1.tif
        if test -f "$FILE"; then
            echo "$FILE exists."
        else
            gdal_translate -of COG /storage/karger/chelsa_V2/cmip6_5km_mcb/5km/${SOURCE_ID}_${EXPERIMENT_ID}_gst_${YEAR}.V.2.1.tif /storage/karger/chelsa_V2/cmip6_5km_mcb/COG/${SOURCE_ID}_${EXPERIMENT_ID}_gst_${YEAR}.V.2.1.tif 
        fi

        FILE=/storage/karger/chelsa_V2/cmip6_5km_mcb/COG/${SOURCE_ID}_${EXPERIMENT_ID}_tmin_${YEAR}.V.2.1.tif
        if test -f "$FILE"; then
            echo "$FILE exists."
        else
            gdal_translate -of COG /storage/karger/chelsa_V2/cmip6_5km_mcb/5km/${SOURCE_ID}_${EXPERIMENT_ID}_tmin_${YEAR}.V.2.1.tif /storage/karger/chelsa_V2/cmip6_5km_mcb/COG/${SOURCE_ID}_${EXPERIMENT_ID}_tmin_${YEAR}.V.2.1.tif 
        fi

        FILE=/storage/karger/chelsa_V2/cmip6_5km_mcb/COG/${SOURCE_ID}_${EXPERIMENT_ID}_tlh_${YEAR}.V.2.1.tif
        if test -f "$FILE"; then
            echo "$FILE exists."
        else
            gdal_translate -of COG /storage/karger/chelsa_V2/cmip6_5km_mcb/5km/${SOURCE_ID}_${EXPERIMENT_ID}_tlh_${YEAR}.V.2.1.tif /storage/karger/chelsa_V2/cmip6_5km_mcb/COG/${SOURCE_ID}_${EXPERIMENT_ID}_tlh_${YEAR}.V.2.1.tif 
        fi
    done
  done
fi

done



# set the fixed parameters
INPUT='/media/karger/bg1/obs_gsl_int/'
OUTPUT='/media/karger/bg1/COG/gsl/'

for YEAR in {1980..2018}
do
echo ${YEAR}
for VAR in gsl #tlh gsl gst tmin
do
echo ${VAR}
 cp ${INPUT}CHELSA_obs_${VAR}_${YEAR}.V.2.1.tif /home/karger/scratch/org.tif
 gdal_translate -co "COMPRESS=DEFLATE" -co "PREDICTOR=2" -of COG -a_srs EPSG:4326 /home/karger/scratch/org.tif /home/karger/scratch/tmp.tif
 mv /home/karger/scratch/tmp.tif ${OUTPUT}CHELSA_obs_${VAR}_${YEAR}.V.2.1.tif 
done
done


# set the fixed parameters
INPUT='/media/karger/bg1/missing/'
OUTPUT='/home/karger/scratch/tmp1/'

for YEAR in 2013 2016
do
echo ${YEAR}
for VAR in tlh gsl gst tmin
do
echo ${VAR}
 cp ${INPUT}CHELSA_obs_${VAR}_${YEAR}.V.2.1.tif /home/karger/scratch/org.tif
 gdal_translate -co "COMPRESS=DEFLATE" -co "PREDICTOR=2" -of COG -a_srs EPSG:4326 /home/karger/scratch/org.tif /home/karger/scratch/tmp.tif
 mv /home/karger/scratch/tmp.tif ${OUTPUT}CHELSA_obs_${VAR}_${YEAR}.V.2.1.tif 
done
done

for YEAR in 2013 2016
do
gdal_calc.py -A CHELSA_obs_gst_${YEAR}.V.2.1.tif --outfile=result.tif --calc="A*10" --type=Int16
gdal_translate -a_scale 0.1 -a_offset 0 -co "COMPRESS=DEFLATE" -co "PREDICTOR=2" -of COG -a_srs EPSG:4326 result.tif CHELSA_obs_gst_${YEAR}.V.2.1.COG.tif 
rm result.tif
done



# set the fixed parameters
INPUT='/media/karger/bg1/obs_tlh_cog/'
OUTPUT='/media/karger/bg1/obs_tlh_cog_Int16/'

for YEAR in {1980..2018} #2017 2018
do
echo ${YEAR}
for VAR in tlh #gsl gst tmin
do
echo ${VAR}
 cp ${INPUT}CHELSA_obs_${VAR}_${YEAR}.V.2.1.tif /home/karger/scratch/org.tif
 gdal_translate -ot Int16 -co "COMPRESS=DEFLATE" -co "PREDICTOR=2" -of COG -a_srs EPSG:4326 /home/karger/scratch/org.tif /home/karger/scratch/tmp.tif
 mv /home/karger/scratch/tmp.tif ${OUTPUT}CHELSA_obs_${VAR}_${YEAR}.V.2.1.tif 
done
done


gdal_calc.py -A /home/karger/scratch/Minimum.tif --outfile=result.tif --calc="A*10" --type=Int16
gdal_translate -a_scale 0.1 -a_offset 0 -co "COMPRESS=DEFLATE" -co "PREDICTOR=2" -of COG -a_srs EPSG:4326 result.tif CHELSA_obs_tmin_1981-2010.V.2.1.COG.tif 



