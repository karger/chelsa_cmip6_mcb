sbatch --array=1-251%15 --cpus-per-task=20 --mem-per-cpu=3G --partition=node /home/karger/scripts/chelsa_cmip6_mcb/chelsa_cmip6_mcb.sh

# submit the years
for year in {1980..2019}
do
echo ${year}
singularity exec -B /storage /home/karger/singularity/chelsa_V2.1.ismip.cont python ~/scripts/chelsa_cmip6_mcb/classes/get_treelim_1km.py -y ${year} -i '/storage/karger/chelsa_V2/scaled/' -o '/storage/karger/chelsa_V2/scaled/' -t False
done

# submit the frost
sbatch --cpus-per-task=60 --mem-per-cpu=3G ~/scripts/chelsa_cmip6_mcb/slurm/run_get_frost.sh

