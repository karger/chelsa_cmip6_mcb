#!/bin/bash
#SBATCH --job-name=cmip6mcb
#SBATCH --chdir=/home/karger/
#SBATCH --qos=normal
#SBATCH --account=node
#SBATCH --mail-user=dirk.karger@wsl.ch
#SBATCH --ntasks=1
#SBATCH --time=696:24:15
#SBATCH --output=logs/cmip6mcb_%A_%a.out

# set the fixed parameters
ISIMIP='/storage/karger/ISIMIP3b/'
OUTPUT='/storage/karger/chelsa_V2/cmip6_5km_mcb/5km/'
YEAR=$(date -d "1849-06-15 $SLURM_ARRAY_TASK_ID years" +%Y)

for YEAR in {1850..2100}
do
echo ${YEAR}

if ((${YEAR}<2016))
  then
  EXPERIMENT_ID='historical'
  for SOURCE_ID in 'ukesm1-0-ll' 'gfdl-esm4' 'ipsl-cm6a-lr' 'mpi-esm1-2-hr' 'mri-esm2-0'
  do
  count=`ls -1 /storage/karger/chelsa_V2/cmip6_5km_mcb/5km/${SOURCE_ID}_historical_*_${YEAR}* 2>/dev/null | wc -l`
  if (( ${count} < 4 ));
    then
      echo "files missing! calculating" 
      mkdir /home/karger/scratch/${SOURCE_ID}${EXPERIMENT_ID}${YEAR}
      TEMP=/home/karger/scratch/${SOURCE_ID}${EXPERIMENT_ID}${YEAR}/
      # calculate climatologies
      singularity exec -B /storage /home/karger/singularity/chelsa_cmip6.sif python3 ~/scripts/chelsa_cmip6_mcb/classes/get_climatologies.py --source_id ${SOURCE_ID} --experiment_id ${EXPERIMENT_ID} --year ${YEAR} --input ${ISIMIP} --output ${TEMP}
      # calculate mcb parameters
      singularity exec -B /storage /home/karger/singularity/chelsa_V2.1.ismip.cont python ~/scripts/chelsa_cmip6_mcb/classes/get_treelim.py --source_id ${SOURCE_ID} --experiment_id ${EXPERIMENT_ID} --year ${YEAR} --input ${TEMP} --output ${OUTPUT}
      rm -r ${TEMP}
    else
      echo "all good"
  fi
  done
else
  for SOURCE_ID in 'gfdl-esm4' 'ipsl-cm6a-lr' 'mpi-esm1-2-hr' 'mri-esm2-0' 'ukesm1-0-ll'
  do
    for EXPERIMENT_ID in 'ssp370' 'ssp126'  'ssp585'
    do
      count=`ls -1 /storage/karger/chelsa_V2/cmip6_5km_mcb/5km/${SOURCE_ID}_${EXPERIMENT_ID}_*_${YEAR}* 2>/dev/null | wc -l`
  if (( ${count} < 4 ));
    then
      echo "files missing! calculating" 
      mkdir /home/karger/scratch/${SOURCE_ID}${EXPERIMENT_ID}${YEAR}
      TEMP=/home/karger/scratch/${SOURCE_ID}${EXPERIMENT_ID}${YEAR}/
      # calculate climatologies
      singularity exec -B /storage /home/karger/singularity/chelsa_cmip6.sif python3 ~/scripts/chelsa_cmip6_mcb/classes/get_climatologies.py --source_id ${SOURCE_ID} --experiment_id ${EXPERIMENT_ID} --year ${YEAR} --input ${ISIMIP} --output ${TEMP}
      # calculate mcb parameters
      singularity exec -B /storage /home/karger/singularity/chelsa_V2.1.ismip.cont python ~/scripts/chelsa_cmip6_mcb/classes/get_treelim.py --source_id ${SOURCE_ID} --experiment_id ${EXPERIMENT_ID} --year ${YEAR} --input ${TEMP} --output ${OUTPUT}
      rm -r ${TEMP}
    else
      echo "all good"
  fi
    done
  done
fi

done

