#!/usr/bin/env python

#This file is part of chelsa_cmip6_mcb.
#
#chelsa_cmip6_mcb is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#chelsa_cmip6 is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with chelsa_cmip6_mcb.  If not, see <https://www.gnu.org/licenses/>.


import xarray as xr
import glob
import argparse

ap = argparse.ArgumentParser(
    description='''# This script creates monthly high-resolution 
    for min-, max-, and mean temperature, precipitation rate 
    climatologies from anomalies and using CHELSA V2.1 as 
    baseline high resolution climatology.
    ''',
    epilog='''author: Dirk N. Karger, dirk.karger@wsl.ch'''
)
ap.add_argument('-m', '--source_id', type=str,
                help="Source model (GCM), e.g. gfdl-esm4, string")
ap.add_argument('-s', '--experiment_id', type=str,
                help="Experiment ID, e.g. ssp585, string")
ap.add_argument('-y', '--year', type=int,
                help="year, e.g. 2040, integer")
ap.add_argument('-i', '--input', type=str,
                help="input directory, input data root path, string")
ap.add_argument('-o', '--output', type=str,
                help="output directory, needs to exist, string")

args = ap.parse_args()
print(args)


model = args.source_id #'gfdl-esm4'
ssp = args.experiment_id #'ssp126'
path = args.input #'/storage/karger/ISIMIP3b/'
year = args.year #1870
output = args.output

debug = 0
if debug == 1:
    model = 'gfdl-esm4'
    ssp = 'ssp126'
    path = '/storage/karger/ISIMIP3b/'
    year = 1870
    output = '/home/karger/scratch/' #'/storage/karger/chelsa_v2/cmip6_5km_mcb/'

def _get_chelsa5(var, path):
    """
    Get the chelsa dataset
    :param var: variable name
    :param path: root directory for the files
    :return: xarray
    """
    filename = path + 'CHELSA_' + var + '_1981-2010_V.2.1.nc'
    ds = xr.open_mfdataset(filename)

    return ds


class interpol:
    """
    Spatial interpolation class for xarray datasets

    :param ds: an xarray dataset
    :param template: a xarray dataset with the target resolution
    :return: a spatially, to the resoluton of the template, interpolated xarray
    :rtype: xarray
    """

    def __init__(self, ds, template):
        self.ds = ds
        self.template = template

    def interpolate(self):
        res = self.ds.interp(lat=self.template["lat"], lon=self.template["lon"])
        return res


def clim_isimip(model, ssp, var, year, chelsa, path):
    """
    Calculates delta change climatologies

    :param model: isimip model name
    :param ssp: socio-economic pathway
    :param var: variable name
    :param template: template dataset to interpolate onto
    :param path: root directory for the files
    :return: xarray
    """
    file_names = glob.glob(path + ssp + '/' + model.upper() + '/' + model + '_r1i1p1f1_w5e5_' + ssp + '_' + var + '_global_daily_*.nc')
    file_names = [x for x in file_names if "norm" not in x]
    print(file_names)
    file_names_hist = glob.glob(path + 'historical/' + model.upper() + '/' + model + '_r1i1p1f1_w5e5_historical_' + var + '_global_daily_*.nc')
    file_names_hist = [x for x in file_names_hist if "norm" not in x]
    print(file_names_hist)
    ds = xr.open_mfdataset([file_names_hist, file_names])
    ds_fut = ds.sel(time=slice(str(year) + '-01-01', str(year) + '-12-31')).groupby("time.month").mean("time")
    ds_ref = ds.sel(time=slice('1981-01-01', '2010-01-31')).groupby("time.month").mean("time")

    anom = None
    clim = None

    if var == 'tas' or var == 'tasmin' or var == 'tasmax':
        anom = ds_fut - ds_ref

    if var == 'pr':
        anom = (ds_fut + 0.000001) / (ds_ref + 0.000001)

    #if var == 'tasmin' or var == 'tasmax' or var == 'pr':
    chelsa = chelsa.rename({'time': 'month'})

    anom_int = interpol(anom, chelsa).interpolate()

    if var == 'tas' or var == 'tasmin' or var == 'tasmax':
        clim = chelsa + anom_int

    if var == 'pr':
        clim = chelsa * anom_int

    return clim


def get_climatologies():
    """

    :return: True
    """
    for var in ['tas', 'tasmax', 'tasmin', 'pr']:
        chelsa = _get_chelsa5(var, '/storage/karger/chelsa_V2/scaled/1981-2010/5km/')
        isimip_clim = clim_isimip(model, ssp, var, year, chelsa, path)
        isimip_clim.to_netcdf(output + model + '_' + ssp + '_' + var + '_' + str(year) + '.nc')

    return True


if __name__ == '__main__':
    get_climatologies()
