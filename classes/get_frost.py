#!/usr/bin/env python

#This file is part of chelsa_cmip6_mcb.
#
#chelsa_cmip6_mcb is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#chelsa_cmip6 is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with chelsa_cmip6_mcb.  If not, see <https://www.gnu.org/licenses/>.

"""
------------------------------------------------------------------------------------------------s-------------
his script creates monthly high-resolution for min-, max-, and mean temperature, precipitation rate
climatologies from anomalies and using CHELSA V2.1 as baseline high resolution climatology. Only works for GCMs for
which tas, tasmax, tasmin, and pr are available.
-------------------------------------------------------------------------------------------------------------
"""

import argparse
import saga_api
import sys
import os
import os.path

ap = argparse.ArgumentParser(
    description='''# This script creates monthly high-resolution 
    for min-, max-, and mean temperature, precipitation rate 
    climatologies from anomalies and using CHELSA V2.1 as 
    baseline high resolution climatology. Only works for GCMs for
    which tas, tasmax, tasmin, and pr are available.
    ''',
    epilog='''author: Dirk N. Karger, dirk.karger@wsl.ch'''
)
ap.add_argument('-m', '--source_id', type=str,
                help="Source model (GCM), e.g. gfdl-esm4, string")
ap.add_argument('-s', '--experiment_id', type=str,
                help="Experiment ID, e.g. ssp585, string")
ap.add_argument('-y', '--years', type=str,
                help="years, e.g. 2041-2070, integer")
ap.add_argument('-i', '--input', type=str,
                help="input directory, input data root path, string")
ap.add_argument('-o', '--output', type=str,
                help="output directory, needs to exist, string")

args = ap.parse_args()
print(args)


year = args.years
output = args.output
input = args.input
ssp = args.experiment_id
model = args.source_id

debug = 0
if debug == 1:
    year = '2041-2070'
    model = 'gfdl-esm4'
    ssp = 'ssp126'
    input = '/storage/karger/chelsa_V2/scaled/' #'/storage/karger/chelsa_v2/cmip6_5km_mcb/'
    output = '/storage/karger/chelsa_V2/scaled/' + year + '/' + model + '/' + ssp + '/bio/'

# ********************************************************
# Define functions needed for the calculations
# ********************************************************
def Load_Tool_Libraries(Verbose):
    saga_api.SG_UI_Msg_Lock(True)
    if os.name == 'nt':    # Windows
        os.environ['PATH'] = os.environ['PATH'] + ';' + os.environ['SAGA_32'] + '/dll'
        saga_api.SG_Get_Tool_Library_Manager().Add_Directory(os.environ['SAGA_32' ] + '/tools', False)
    else:                  # Linux
        saga_api.SG_Get_Tool_Library_Manager().Add_Directory('/usr/local/lib/saga/', False)        # Or set the Tool directory like this!
    saga_api.SG_UI_Msg_Lock(False)

    if Verbose == True:
                print 'Python - Version ' + sys.version
                print saga_api.SAGA_API_Get_Version()
                print 'number of loaded libraries: ' + str(saga_api.SG_Get_Tool_Library_Manager().Get_Count())
                print

    return saga_api.SG_Get_Tool_Library_Manager().Get_Count()


def import_gdal(File):
    #_____________________________________
    # Create a new instance of tool 'Import Raster'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('io_gdal', '0')
    if Tool == None:
        print 'Failed to create tool: Import Raster'
        return False

    Parm = Tool.Get_Parameters()
    Parm('FILES').Set_Value(File)
    Parm('MULTIPLE').Set_Value('automatic')
    Parm('TRANSFORM').Set_Value(False)
    Parm('RESAMPLING').Set_Value('Nearest Neighbour')

    print 'Executing tool: ' + Tool.Get_Name().c_str()
    if Tool.Execute() == False:
        print 'failed'
        return False
    print 'okay'

    #_____________________________________
    output = Tool.Get_Parameter(saga_api.CSG_String('GRIDS')).asGridList().Get_Grid(0)
    # _____________________________________

    return output


def get_min_dicto(tas):
    #_____________________________________
    # Create a new instance of tool 'Statistics for Grids'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('statistics_grid', '4')
    if Tool == None:
        print('Failed to create tool: Statistics for Grids')
        return False

    Tool.Get_Parameters().Reset_Grid_System()

    for n in range(1,13):
        n = str(n)
        Tool.Get_Parameter('GRIDS').asList().Add_Item(tas['tas'+n])

    Tool.Set_Parameter('RESAMPLING', 'B-Spline Interpolation')
    Tool.Set_Parameter('MIN', saga_api.SG_Get_Create_Pointer()) #

    print('Executing tool: ' + Tool.Get_Name().c_str())
    if Tool.Execute() == False:
        print('failed')
        return False
    print('okay')

    Data = Tool.Get_Parameter('MIN').asDataObject()

    return Data


def export_geotiff(OBJ,outputfile):
    #_____________________________________
    # Create a new instance of tool 'Export GeoTIFF'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('io_gdal', '2')
    if Tool == None:
        print 'Failed to create tool: Export GeoTIFF'
        return False

    Parm = Tool.Get_Parameters()
    Parm.Reset_Grid_System()
    Parm('GRIDS').asList().Add_Item(OBJ)
    Parm('FILE').Set_Value(outputfile)
    Parm('OPTIONS').Set_Value('COMPRESS=DEFLATE PREDICTOR=2')

    print 'Executing tool: ' + Tool.Get_Name().c_str()
    if Tool.Execute() == False:
        print 'failed'
        return False
    print 'okay - geotiff created'

    #_____________________________________
    # remove this tool instance, if you don't need it anymore
    saga_api.SG_Get_Tool_Library_Manager().Delete_Tool(Tool)

    return True


def run_get_min():

    tas = {}
    for month in range(1,13):
        filename = '/storage/karger/chelsa_V2/scaled/' + year + '/' + model.upper() + '/' + ssp + '/tas/CHELSA_' + model + '_r1i1p1f1_w5e5_' + ssp + '_tas_' + str("%02d" % (month,)) + '_' + year.replace('-', '_') + '_norm.tif'
        f1 = import_gdal(filename)
        #f1.Set_Scaling(0.1, -273.15)
        tas["tas{0}".format(month)] = f1

    # calculate frost
    tmin = get_min_dicto(tas)

    export_geotiff(tmin, output + '/CHELSA_' + model + '_r1i1p1f1_w5e5_' + ssp + '_tmin_' + year.replace('-', '_') + '_norm.tif')


if __name__ == '__main__':
    Load_Tool_Libraries(True)
    run_get_min()
