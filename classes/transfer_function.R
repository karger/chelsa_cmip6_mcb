# calculate a transfer function between gst and gsl

library(terra)
year = 1981

# build a spatio temporal data frame containing all gsl and gst values over 300 mountain regions 1981-2010
dfout <- data.frame(gsl=numeric(), gst=numeric(), year=numeric(), region=numeric())
for (year in seq(1981,2010,1)){
    print(year)
    mask <- rast("/home/karger/scratch/GMBA_Definition_v2.0.tif")
    regions <- rast("/home/karger/scratch/GMBA_regions_300_gridded.tif")
    gsl <- rast(paste0("/media/karger/bg1/obs_mcb_mol/CHELSA_obs_gsl_",year,".V.2.1.tif"))
    gst <- rast(paste0("/media/karger/bg1/obs_mcb_mol/CHELSA_obs_gst_",year,".V.2.1.tif"))
    gsl[is.na(mask)] <- NA
    gst[is.na(mask)] <- NA
    rs <- data.frame(gsl=values(gsl), gst=values(gst), year=year, region=values(regions))
    rs <- rs[complete.cases(rs),]
    colnames(rs) <- c("gsl","gst","year", "region")
    dfout <- rbind(dfout, rs)
    colnames(dfout) <- c("gsl","gst","year", "region")
}
write.table(dfout, "/home/karger/scratch/chelsa_gst_gsl_1981-2010_regions.txt")


# read in the data
library(data.table)
dfout <- data.table::fread("/home/karger/scratch/chelsa_gst_gsl_1981-2010_regions.txt")
dfout <- dfout[complete.cases(dfout), ]
dfout2 <- dfout[,2:5]

region_ids <- unique(values(regions))

# loop over all mountain regions and fit a linear model betweeen gsl and gst in log log space
reg_v <- c()
coeff1 <- c()
coeff2 <- c()
for (n in 2:292){
reg_id <- region_ids[n]
rs1 <- dfout2[dfout2$region == reg_id,]
print(reg_id)
plot(rs1$gst, rs1$gsl)
#plot(log(rs1$gst), log(rs1$gsl))
#cor(log(rs1$gst), log(rs1$gsl))
lm1 <- lm(gsl ~ gst,data=rs1)#, family=poisson(link=log))
abline(lm1, col="red")
reg_v <- c(reg_v, reg_id)
coeff1 <- c(coeff1, coefficients(lm1)[1])
coeff2 <- c(coeff2, coefficients(lm1)[2])
}

# write out the parameters
resdf <- as.data.frame(cbind(reg_v, coeff1))
regions_coeef <- regions
for (id in unique(resdf$reg_v)){
    print(id)
    regions_coeef[values(regions_coeef) == id] <- resdf[resdf$reg_v == id, 2] 
}
writeRaster(regions_coeef, file="/home/karger/scratch/lm_gsl_gst_coeff1.tif", datatype = 'FLT4S')


# write out the parameters
resdf <- as.data.frame(cbind(reg_v, coeff2))
regions_coeef <- regions
for (id in unique(resdf$reg_v)){
    print(id)
    regions_coeef[values(regions_coeef) == id] <- resdf[resdf$reg_v == id, 2] 
}
writeRaster(regions_coeef, file="/home/karger/scratch/lm_gsl_gst_coeff2.tif", datatype = 'FLT4S')







st1 <- rast()
for (year in seq(1981,2010,1)){
    print(year)
    ls <- list.files(path='/storage/karger/chelsa_V2/OUTPUT_DAILY/tz/',pattern=paste0("_",year), full.names=T)
    r1 <- rast(ls)
    mm <- terra::app(r1, mean, cores=38)
    st1 <- c(st1, mm)
}
mm1 <- terra::app(st1, mean, cores=38)
writeRaster(mm1, file="/home/karger/scratch/CHELSA_tz_1981-2010.V.2.1.tif", datatype = 'FLT4S')













rs1 <- rs[rs$CHELSA_obs_gsl_1981.V.2.1 != 365, ]

lm1 <- lm(log(rs1$CHELSA_obs_gsl_1981.V.2.1)~log(rs1$CHELSA_obs_gst_1981.V.2.1))
rs1_ds <- rs1[sample(nrow(rs1), 555000), ]
plot(rs1_ds$CHELSA_obs_gsl_1981.V.2.1~rs1_ds$CHELSA_obs_gst_1981.V.2.1, cex=0.1)
lm1 <- lm(rs1_ds$CHELSA_obs_gsl_1981.V.2.1~rs1_ds$CHELSA_obs_gst_1981.V.2.1)
summary(lm1)
abline(lm1)

range01 <- function(x){
    (x-min(x))/(max(x)-min(x))}

rs2 <- rs1_ds

rs2$CHELSA_obs_gsl_1981.V.2.1 <- range01(rs2$CHELSA_obs_gsl_1981.V.2.1)

colnames(rs2) <- c("gsl","gst","year")

plot(rs2$gsl~rs2$gst, cex=0.1)
glm1 <- lm(gsl ~ 0 + gst,data=rs2)#, family=poisson(link=log))
vec <- seq(0,35,0.1)
vec <- as.data.frame(vec)
colnames(vec) <- c("gst")
vec$pred <- predict(glm1, vec, type="response")
points(vec$gst,vec$pred, col="red")

