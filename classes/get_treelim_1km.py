#!/usr/bin/env python

#This file is part of chelsa_cmip6_mcb.
#
#chelsa_cmip6_mcb is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#chelsa_cmip6 is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with chelsa_cmip6_mcb.  If not, see <https://www.gnu.org/licenses/>.


import argparse
import saga_api
import sys
import os
import os.path

ap = argparse.ArgumentParser(
    description='''# This script runs the treeline model based on  CHELSA V2.1,
    the output is the growing season length (gsl), mean temperature of the growing season (gst),
    potential treeline height, as difference to the reference dem GMTED2010 (Danielson, J. J. and Gesch, D. B.: 
    Global multi-resolution terrain elevation data 2010 (GMTED2010), US Geological Survey, 2011.), and the minimum
    daily mean 2m air temperature through the year (tmin). 
    ''',
    epilog='''author: Dirk N. Karger, dirk.karger@wsl.ch'''
)

ap.add_argument('-y', '--year', type=int,
                help="year, e.g. 2005, integer")
ap.add_argument('-i', '--input', type=str,
                help="input directory, input data root path, string")
ap.add_argument('-o', '--output', type=str,
                help="output directory, needs to exist, string")
ap.add_argument('-t', '--tlh', type=bool,
                help="shall the treeline be calculated, boolean")

args = ap.parse_args()
print(args)



year = args.year #1870
output = args.output
input = args.input

debug = 0
if debug == 1:
    year = 2005
    input = '/storage/karger/chelsa_V2/scaled/' #'/storage/karger/chelsa_v2/cmip6_5km_mcb/'
    output = '/storage/karger/chelsa_V2/cmip6_5km_mcb/1km/'

# ********************************************************
# Define functions needed for the calculations
# ********************************************************
def Load_Tool_Libraries(Verbose):
    saga_api.SG_UI_Msg_Lock(True)
    if os.name == 'nt':    # Windows
        os.environ['PATH'] = os.environ['PATH'] + ';' + os.environ['SAGA_32'] + '/dll'
        saga_api.SG_Get_Tool_Library_Manager().Add_Directory(os.environ['SAGA_32' ] + '/tools', False)
    else:                  # Linux
        saga_api.SG_Get_Tool_Library_Manager().Add_Directory('/usr/local/lib/saga/', False)        # Or set the Tool directory like this!
    saga_api.SG_UI_Msg_Lock(False)

    if Verbose == True:
                print 'Python - Version ' + sys.version
                print saga_api.SAGA_API_Get_Version()
                print 'number of loaded libraries: ' + str(saga_api.SG_Get_Tool_Library_Manager().Get_Count())
                print

    return saga_api.SG_Get_Tool_Library_Manager().Get_Count()


def import_ncdf(ncdffile):
    #_____________________________________
    # Create a new instance of tool 'Import NetCDF'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('io_gdal', '6')
    if Tool == None:
        print 'Failed to create tool: Import NetCDF'
        return False

    Parm = Tool.Get_Parameters()
    Parm('FILE').Set_Value(ncdffile)
    Parm('SAVE_FILE').Set_Value(False)
    Parm('SAVE_PATH').Set_Value('')
    Parm('TRANSFORM').Set_Value(True)
    Parm('RESAMPLING').Set_Value('Nearest Neighbour')

    print 'Executing tool: ' + Tool.Get_Name().c_str()
    if Tool.Execute() == False:
        print 'failed'
        return False
    print 'okay'

    #_____________________________________

    output = Tool.Get_Parameter(saga_api.CSG_String('GRIDS')).asGridList()
    return output


def import_gdal(File):
    #_____________________________________
    # Create a new instance of tool 'Import Raster'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('io_gdal', '0')
    if Tool == None:
        print 'Failed to create tool: Import Raster'
        return False

    Parm = Tool.Get_Parameters()
    Parm('FILES').Set_Value(File)
    Parm('MULTIPLE').Set_Value('automatic')
    Parm('TRANSFORM').Set_Value(False)
    Parm('RESAMPLING').Set_Value('Nearest Neighbour')

    print 'Executing tool: ' + Tool.Get_Name().c_str()
    if Tool.Execute() == False:
        print 'failed'
        return False
    print 'okay'

    #_____________________________________
    output = Tool.Get_Parameter(saga_api.CSG_String('GRIDS')).asGridList().Get_Grid(0)
    # _____________________________________

    return output


def get_min_dicto(tas):
    #_____________________________________
    # Create a new instance of tool 'Statistics for Grids'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('statistics_grid', '4')
    if Tool == None:
        print('Failed to create tool: Statistics for Grids')
        return False

    Tool.Get_Parameters().Reset_Grid_System()

    for n in range(1,13):
        n = str(n)
        Tool.Get_Parameter('GRIDS').asList().Add_Item(tas['tas'+n])

    Tool.Set_Parameter('RESAMPLING', 'B-Spline Interpolation')
    Tool.Set_Parameter('MIN', saga_api.SG_Get_Create_Pointer()) #

    print('Executing tool: ' + Tool.Get_Name().c_str())
    if Tool.Execute() == False:
        print('failed')
        return False
    print('okay')

    Data = Tool.Get_Parameter('MIN').asDataObject()

    return Data


def treelim(tas, tasmin, tasmax, pr):
    #_____________________________________
    # Create a new instance of tool 'Tree Growth Season'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('climate_tools', '11')
    if Tool == None:
        print('Failed to create tool: Tree Growth Season')
        return False

    Tool.Get_Parameters().Reset_Grid_System()

    for n in range(1,13):
        n = str(n)
        Tool.Get_Parameter('T').asList().Add_Item(tas['tas'+n])
        Tool.Get_Parameter('TMIN').asList().Add_Item(tasmin['tasmin'+n])
        Tool.Get_Parameter('TMAX').asList().Add_Item(tasmax['tasmax'+n])
        Tool.Get_Parameter('P').asList().Add_Item(pr['pr'+n])
    #Tool.Set_Parameter('SWC', saga_api.SG_Get_Data_Manager().Add(swc))
    Tool.Set_Parameter('SWC_DEFAULT', 220.000000)
    Tool.Set_Parameter('SWC_SURFACE', 30.000000)
    Tool.Set_Parameter('SW1_RESIST', 0.500000)
    Tool.Set_Parameter('LAT_DEF', 0.000000)
    Tool.Set_Parameter('TLH', saga_api.SG_Get_Create_Pointer()) # optional output, remove this line, if you don't want to create it
    Tool.Set_Parameter('DT_MIN', 0.900000)
    Tool.Set_Parameter('SW_MIN', 2.000000)
    Tool.Set_Parameter('LGS_MIN', 94)
    Tool.Set_Parameter('SMT_MIN', 6.400000)
    Tool.Set_Parameter('TLH_MAX_DIFF', 5000.000000)

    print('Executing tool: ' + Tool.Get_Name().c_str())
    if Tool.Execute() == False:
        print('failed')
        return False
    print('okay')

    gst = Tool.Get_Parameter('SMT').asDataObject()
    gsl = Tool.Get_Parameter('LGS').asDataObject()
    tlh = Tool.Get_Parameter('TLH').asDataObject()

    return gst, gsl, tlh


def calc_gsl(tas, tasmin, tasmax, pr, dt_min, swc):
    #_____________________________________
    # Create a new instance of tool 'Tree Growth Season'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('climate_tools', '11')
    if Tool == None:
        print('Failed to create tool: Tree Growth Season')
        return False

    Tool.Get_Parameters().Reset_Grid_System()

    for n in range(1,13):
        n = str(n)
        Tool.Get_Parameter('T').asList().Add_Item(tas['tas'+n])
        Tool.Get_Parameter('TMIN').asList().Add_Item(tasmin['tasmin'+n])
        Tool.Get_Parameter('TMAX').asList().Add_Item(tasmax['tasmax'+n])
        Tool.Get_Parameter('P').asList().Add_Item(pr['pr'+n])

    Tool.Set_Parameter('SWC', saga_api.SG_Get_Data_Manager().Add(swc))

    Tool.Set_Parameter('SWC_DEFAULT', 220.000000)
    Tool.Set_Parameter('SWC_SURFACE', 30.000000)
    Tool.Set_Parameter('SW1_RESIST', 0.500000)
    Tool.Set_Parameter('LAT_DEF', 0.000000)
    Tool.Set_Parameter('DT_MIN', dt_min)
    Tool.Set_Parameter('SW_MIN', 2.000000)
    Tool.Set_Parameter('LGS_MIN', 94)
    Tool.Set_Parameter('SMT_MIN', 6.400000)

    print('Executing tool: ' + Tool.Get_Name().c_str())
    if Tool.Execute() == False:
        print('failed')
        return False
    print('okay')

    gst = Tool.Get_Parameter('SMT').asDataObject()
    gsl = Tool.Get_Parameter('LGS').asDataObject()

    return gst, gsl


def get_min(tas):
    #_____________________________________
    # Create a new instance of tool 'Statistics for Grids'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('statistics_grid', '4')
    if Tool == None:
        print('Failed to create tool: Statistics for Grids')
        return False

    Tool.Get_Parameters().Reset_Grid_System()

    for n in range(0,12):
        Tool.Get_Parameter('GRIDS').asList().Add_Item(tas.Get_Grid(n))

    Tool.Set_Parameter('RESAMPLING', 'B-Spline Interpolation')
    Tool.Set_Parameter('MIN', saga_api.SG_Get_Create_Pointer()) # optional output, remove this line, if you don't want to create it

    print('Executing tool: ' + Tool.Get_Name().c_str())
    if Tool.Execute() == False:
        print('failed')
        return False
    print('okay')

    Parm = Tool.Get_Parameters()
    Data = Parm('MIN').asGrid()

    return Data


def export_geotiff(OBJ,outputfile):
    #_____________________________________
    # Create a new instance of tool 'Export GeoTIFF'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('io_gdal', '2')
    if Tool == None:
        print 'Failed to create tool: Export GeoTIFF'
        return False

    Parm = Tool.Get_Parameters()
    Parm.Reset_Grid_System()
    Parm('GRIDS').asList().Add_Item(OBJ)
    Parm('FILE').Set_Value(outputfile)
    Parm('OPTIONS').Set_Value('COMPRESS=DEFLATE PREDICTOR=2')

    print 'Executing tool: ' + Tool.Get_Name().c_str()
    if Tool.Execute() == False:
        print 'failed'
        return False
    print 'okay - geotiff created'

    #_____________________________________
    # remove this tool instance, if you don't need it anymore
    saga_api.SG_Get_Tool_Library_Manager().Delete_Tool(Tool)

    return True


def run_treelim(calc_tlh=False):

    tas = {}
    for month in range(1,13):
        filename = '/storage/karger/chelsa_V2/OUTPUT_MONTHLY/tas/' + 'CHELSA_tas_' + str("%02d" % (month,)) + '_' + str(year) + '_V.2.1.tif'
        f1 = import_gdal(filename)
        f1.Set_Scaling(0.1, -273.15)
        tas["tas{0}".format(month)] = f1

    tasmax = {}
    for month in range(1,13):
        filename = '/storage/karger/chelsa_V2/OUTPUT_MONTHLY/tasmax/' + 'CHELSA_tasmax_' + str("%02d" % (month,)) + '_' + str(year) + '_V.2.1.tif'
        f1 = import_gdal(filename)
        f1.Set_Scaling(0.1, -273.15)
        tasmax["tasmax{0}".format(month)] = f1

    tasmin = {}
    for month in range(1,13):
        filename = '/storage/karger/chelsa_V2/OUTPUT_MONTHLY/tasmin/' + 'CHELSA_tasmin_' + str("%02d" % (month,)) + '_' + str(year) + '_V.2.1.tif'
        f1 = import_gdal(filename)
        f1.Set_Scaling(0.1, -273.15)
        tasmin["tasmin{0}".format(month)] = f1

    pr = {}
    for month in range(1,13):
        filename = '/storage/karger/chelsa_V2/OUTPUT_MONTHLY/pr/' + 'CHELSA_pr_' + str("%02d" % (month,)) + '_' + str(year) + '_V.2.1.tif'
        f1 = import_gdal(filename)
        f1.Set_Scaling(0.01, 0)
        pr["pr{0}".format(month)] = f1


    #awc = import_gdal('/storage/karger/awc_l1-6.tif')

    gst = None
    gsl = None

    # calculate treelim
    if calc_tlh == True:
        gst, gsl, tlh = treelim(tas = tas,
                                tasmin = tasmin,
                                tasmax = tasmax,
                                pr = pr)
                                #swc = awc)

        export_geotiff(tlh, output + '/CHELSA_obs_tlh_' + str(year) + '.V.1.0.tif')

    # calculate gsl only
    if calc_tlh == False:
        gst, gsl, = calc_gsl(tas = tas,
                             tasmin = tasmin,
                             tasmax = tasmax,
                             pr = pr,
                             dt_min = 0.900000)
                             #,
                             #swc = awc)

    # calculate frost
    tmin = get_min_dicto(tas)

    export_geotiff(gst, output + '/CHELSA_obs_gst_' + str(year) + '.V.2.1.tif')
    #os.system('gdal_calc.py --type=Uint16 --calc=\'(A+273.15)*10\' -A /home/karger/scratch/tmp2.tif --outfile=/home/karger/scratch/tmp2.tif')
    #os.system("gdal_translate -a_scale 0.1 -a_offset -273.15 -a_nodata 0 -co COMPRESS=DEFLATE -co PREDICTOR=2 -ot Uint16 /home/karger/scratch/tmp2.tif " + output + '/CHELSA_obs_gst_' + str(year) + '.V.1.0.tif')
    export_geotiff(gsl, output + '/CHELSA_obs_gsl_' + str(year) + '.V.2.1.tif')
    export_geotiff(tmin, output + '/CHELSA_obs_tmin_' + str(year) + '.V.2.1.tif')


if __name__ == '__main__':
    Load_Tool_Libraries(True)
    run_treelim(calc_tlh=args.tlh)
