#!/bin/bash

#SBATCH --job-name=TREELIM
#SBATCH -A node # Node Account
#SBATCH --qos normal # normal priority level
#SBATCH --mail-user=dirk.karger@wsl.ch
#SBATCH --mail-type=SUBMIT,END,FAIL
#SBATCH --output=/home/karger/logs/frost_%A_%a.out.out
#SBATCH --time=480:00:00
#SBATCH --ntasks=1
#SBATCH --array=1


for TIME in "2011_2040" "2041_2070" "2071_2100"
    do
    for MODEL in "gfdl-esm4" "ipsl-cm6a-lr" "mpi-esm1-2-hr" "mri-esm2-0" "ukesm1-0-ll"
        do
        for SSP in ssp126 ssp370 ssp585
            do
            singularity exec -B /storage /home/karger/singularity/chelsa_V2.1.ismip.cont python ~/scripts/chelsa_cmip6_mcb/classes/get_treelim_1km_CMIP6.py -s ${SSP} -m ${MODEL} -tt ${TIME} -i '/storage/karger/chelsa_V2/scaled/' -o '/storage/karger/chelsa_V2/scaled/'
        done
    done
done


