#!/bin/bash

#SBATCH --job-name=BApost
#SBATCH -A node # Node Account
#SBATCH --qos normal # normal priority level
#SBATCH --mail-user=dirk.karger@wsl.ch
#SBATCH --mail-type=SUBMIT,END,FAIL
#SBATCH --output=/home/karger/logs/frost_%A_%a.out.out
#SBATCH --time=480:00:00
#SBATCH --ntasks=1
#SBATCH --array=1-1

INPUT='/storage/karger/chelsa_V2/scaled/'
OUTPUT='/storage/karger/chelsa_V2/scaled/tmin/'
MODEL=$1
SSP=$2
YEAR=$3
singularity exec -B /storage/ /home/karger/singularity/chelsa_V1.2.saga7.9.cont2 python /home/karger/scripts/chelsa_cmip6_mcb/classes/get_frost.py -m ${MODEL} -s ${SSP} -y ${YEAR} -i ${INPUT} -o ${OUTPUT}



