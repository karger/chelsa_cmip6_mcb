for MODEL in gfdl-esm4 ipsl-cm6a-lr mpi-esm1-2-hr mri-esm2-0 ukesm1-0-ll
do
  for SSP in ssp370
  do
    for YEAR in '2011-2040' '2041-2070' '2071-2100'
    do
      sbatch --cpus-per-task=20 --mem-per-cpu=3G --partition=node /home/karger/scripts/chelsa_cmip6_mcb/slurm/run_get_frost.sh ${MODEL} ${SSP} ${YEAR}
    done
  done
done
