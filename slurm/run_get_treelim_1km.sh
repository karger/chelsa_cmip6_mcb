#!/bin/bash

#SBATCH --job-name=TREELIM
#SBATCH -A node # Node Account
#SBATCH --qos normal # normal priority level
#SBATCH --mail-user=dirk.karger@wsl.ch
#SBATCH --mail-type=SUBMIT,END,FAIL
#SBATCH --output=/home/karger/logs/frost_%A_%a.out.out
#SBATCH --time=480:00:00
#SBATCH --ntasks=1
#SBATCH --array=1-10

YEAR=$(date -d "2009-06-15 $SLURM_ARRAY_TASK_ID years" +%Y)

if test -f /storage/karger/chelsa_V2/scaled/CHELSA_obs_gst_${YEAR}.V.2.1.tif; then
    echo "FILE exists."
else
singularity exec -B /storage /home/karger/singularity/chelsa_V2.1.ismip.cont python ~/scripts/chelsa_cmip6_mcb/classes/get_treelim_1km.py -y ${YEAR} -i '/storage/karger/chelsa_V2/scaled/' -o '/storage/karger/chelsa_V2/scaled/' -t True
fi

