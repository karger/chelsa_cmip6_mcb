#!/usr/bin/env python

#This file is part of chelsa_cmip6_mcb.
#
#chelsa_cmip6_mcb is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#chelsa_cmip6 is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with chelsa_cmip6_mcb.  If not, see <https://www.gnu.org/licenses/>.

"""
-------------------------------------------------------------------------------------------------------------
Convert  CHELSA GeoTIFF in a folder to single NetCDF file with time dimension enabled that is CF-Compliant
http://cfconventions.org/cf-conventions/v1.6.0/cf-conventions.html
-------------------------------------------------------------------------------------------------------------
"""

import numpy as np
import datetime as dt
import os
import gdal
import netCDF4
import re

tmp = '/home/karger/scratch/'
for var in ['tas', 'tasmax', 'tasmin', 'pr']:
    for month in range(1,13):
        filename_in = '/storage/karger/chelsa_V2/scaled/1981-2010/' + var + '/CHELSA_' + var + '_' + str(
            "%02d" % (month,)) + '_1981-2010_V.2.1.tif'
        filename_out = tmp + '/CHELSA_' + var + '_' + str("%02d" % (month,)) + '_1981-2010_V.2.1.sgrd'
        os.system('saga_cmd grid_tools 0 -INPUT ' + filename_in + ' -OUTPUT ' + filename_out + ' -SCALE_UP 4 -TARGET_DEFINITION 1 -TARGET_TEMPLATE /storage/karger/chelsa_V2/scaled/1981-2010/5km/CHELSA_tasmax_01_1981-2010_V.2.1.tif')
        filename_in = tmp + '/CHELSA_' + var + '_' + str("%02d" % (month,)) + '_1981-2010_V.2.1.sdat'
        filename_out = tmp + '/CHELSA_' + var + '_' + str("%02d" % (month,)) + '_1981-2010_V.2.1.tif'
        os.system('gdal_translate ' + filename_in + ' ' + filename_out)


for var in ['tasmax', 'tasmin', 'pr']:
    nco = netCDF4.Dataset('/storage/karger/chelsa_V2/scaled/1981-2010/5km/CHELSA_' + var + '_1981-2010_V.2.1.nc', 'w',
                          clobber=True)
    ds = gdal.Open(tmp + '/CHELSA_' + var + '_' + str("%02d" % (month,)) + '_1981-2010_V.2.1.tif')
    a = ds.ReadAsArray()
    nlat, nlon = np.shape(a)

    b = ds.GetGeoTransform()  # bbox, interval
    lon = np.arange(nlon) * b[1] + b[0]
    lat = np.arange(nlat) * b[5] + b[3]

    basedate = dt.datetime(1980, 1, 1, 0, 0, 0)

    # create NetCDF file

    # chunking is optional, but can improve access a lot:
    # (see: http://www.unidata.ucar.edu/blogs/developer/entry/chunking_data_choosing_shapes)
    chunk_lon = 10
    chunk_lat = 10
    chunk_time = 12

    # create dimensions, variables and attributes:
    nco.createDimension('lon', nlon)
    nco.createDimension('lat', nlat)
    nco.createDimension('time', None)

    timeo = nco.createVariable('time', 'f4', ('time'))
    timeo.units = 'month'
    timeo.standard_name = 'time'
    timeo.calendar = 'gregorian'
    timeo.axis = 'T'

    lono = nco.createVariable('lon', 'f4', ('lon'))
    lono.units = 'degrees_east'
    lono.standard_name = 'longitude'
    lono.long_name = 'longitude'
    lono.axis = 'X'

    lato = nco.createVariable('lat', 'f4', ('lat'))
    lato.units = 'degrees_north'
    lato.standard_name = 'latitude'
    lato.long_name = 'latitude'
    lato.axis = 'Y'

    # create container variable for CRS: lon/lat WGS84 datum
    crso = nco.createVariable('crs', 'i4')
    crso.long_name = 'Lon/Lat Coords in WGS84'
    crso.grid_mapping_name = 'latitude_longitude'
    crso.longitude_of_prime_meridian = 0.0
    crso.semi_major_axis = 6378137.0
    crso.inverse_flattening = 298.257223563

    # create short integer variable for precipitation data, with chunking
    pcpo = nco.createVariable(var, 'f4', ('time', 'lat', 'lon'),
                              zlib=True, chunksizes=[chunk_time, chunk_lat, chunk_lon], fill_value=-99999.)
    pcpo.units = 'kg*m**-2*month**-1'
    if var == 'tasmax' or var == 'tasmin' or var == 'tas':
        pcpo.units = 'Celsius'
    if var == 'tasmax':
        pcpo.standard_name = 'Daily 2m Maximum Air Temperature'
    if var == 'tasmin':
        pcpo.standard_name = 'Daily 2m Minimum Air Temperature'
    if var == 'tas':
        pcpo.standard_name = 'Daily 2m Mean Air Temperature'
    if var == 'pr':
        pcpo.standard_name = 'Precipitation rate'

    pcpo.long_name = 'Climatologies at High Resolution for the Earths Land Surface Areas'
    pcpo.time_step = 'climat'
    pcpo.missing_value = -99999.
    pcpo.geospatial_lat_min = -90.
    pcpo.geospatial_lat_max = 90.
    pcpo.geospatial_lon_min = -180.
    pcpo.geospatial_lon_max = 180.
    pcpo.grid_mapping = 'crs'
    pcpo.set_auto_maskandscale(False)

    nco.Conventions = 'CF-1.6'
    nco.title = "CHELSA v2.1"
    nco.history = "created by Swiss Federal Research Institute WSL, Switzerland"
    nco.version = "Version 2.1"
    nco.comments = "time variable denotes the month of the climatological normal."
    nco.website = "https://www.chelsa-climate.org"
    nco.date_created = "2022-07-19"
    nco.creator_name = "Dirk Nikolaus Karger"
    nco.creator_email = "dirk.karger@wsl.ch"
    nco.institution = "Swiss Federal Research Institute WSL"
    nco.note = ""

    # write lon,lat
    lono[:] = lon
    lato[:] = lat
    itime = 0
    for month in range(1,13):
        print(month)
        ds = gdal.Open(tmp + '/CHELSA_' + var + '_' + str("%02d" % (month,)) + '_1981-2010_V.2.1.tif')
        a = ds.ReadAsArray()
        timeo[itime]=month
        pcpo[itime,:,:]=a
        itime = itime + 1

    nco.close()



